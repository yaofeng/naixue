package top.hypnos.bigdata.filesystem.record;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public interface ObjectInputStream {

    int readInt() throws IOException;

    byte[] readBytes() throws IOException;

    default String readString() throws IOException {
        final var bytes = readBytes();
        if (bytes == null) {
            return null;
        }
        return new String(bytes, StandardCharsets.UTF_8);
    }
}
