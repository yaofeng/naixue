package top.hypnos.bigdata.filesystem;

import top.hypnos.bigdata.filesystem.command.CommandException;
import top.hypnos.bigdata.filesystem.record.Record;

public interface Command extends Record {

    int getTxnId();

    String getPath();

    byte[] getData();

    /**
     * 命令预执行
     * @param dataTree
     * @throws CommandException
     */
    void preExecute(MemDataTree dataTree) throws CommandException;

    /**
     * 命令提交
     */
    void commit();
}
