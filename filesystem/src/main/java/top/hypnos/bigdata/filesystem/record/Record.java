package top.hypnos.bigdata.filesystem.record;

import java.io.IOException;

public interface Record {

    void serialize(ObjectOutputStream output) throws IOException;

    void deserialize(ObjectInputStream input) throws IOException;
}
