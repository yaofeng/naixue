package top.hypnos.bigdata.filesystem.command;

public class CommandException extends Exception {

    public CommandException(String message) {
        super(message);
    }
}
