package top.hypnos.bigdata.filesystem.command;

import top.hypnos.bigdata.filesystem.Command;
import top.hypnos.bigdata.filesystem.MemDataNode;
import top.hypnos.bigdata.filesystem.MemDataTree;

public interface CommandExecutor {

    void preExecute(MemDataTree dataTree, Command command) throws CommandException;

    void commit();

    MemDataNode getNode();
}
