package top.hypnos.bigdata.filesystem;

import top.hypnos.bigdata.filesystem.command.CommandException;

import java.io.IOException;

public interface Snapshot {

    void save(MemDataTree memDataTree) throws IOException;

    MemDataTree recover() throws IOException, CommandException;
}
