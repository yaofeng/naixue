package top.hypnos.bigdata.filesystem.commons;

/**
 * 对象打包类，将两个对象打包成一个对象
 * @param <A> 第一个对象
 * @param <B> 第二个对象
 */
public class Pair<A, B> {

    private final A a;

    private final B b;

    private Pair(final A a, final B b) {
        this.a = a;
        this.b = b;
    }

    public static <A, B> Pair<A, B> of(A a, B b) {
        return new Pair<>(a, b);
    }

    public A getA() {
        return a;
    }

    public B getB() {
        return b;
    }

}
