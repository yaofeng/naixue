package top.hypnos.bigdata.filesystem;

import java.io.IOException;
import java.util.Collection;

public interface BinLog {

    /**
     * 向BinLog追加事务操作
     *
     * @param command 事务操作命令队列
     * @return 事务ID
     */
    int append(Command command) throws IOException;

    /**
     * 获取指定事务ID之后的操作命令对象
     *
     * @param txnId 事务ID
     * @return 事务操作命令流
     */
    Collection<Command> loadAfter(int txnId) throws IOException;

    /**
     * 清理指定事务ID之前的事务日志
     *
     * @param txnId 事务ID
     */
    default void clearBefore(int txnId) {
        throw new UnsupportedOperationException("暂不支持事务日志清理操作");
    }
}
