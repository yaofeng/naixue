package top.hypnos.bigdata.filesystem.command;

import top.hypnos.bigdata.filesystem.Command;
import top.hypnos.bigdata.filesystem.MemDataNode;
import top.hypnos.bigdata.filesystem.MemDataTree;

public class DeleteNodeCommandExecutor implements CommandExecutor {

    private MemDataNode node;

    @Override
    public void preExecute(final MemDataTree dataTree, final Command command) throws CommandException {
        final var node = dataTree.getNode(command.getPath());
        if (node == null) {
            throw new CommandException("节点[" + command.getPath() + "]不存在。");
        }
        this.node = node;
    }

    @Override
    public void commit() {
        node.getParent().getChildren().remove(node);
    }

    @Override
    public MemDataNode getNode() {
        return this.node;
    }
}
