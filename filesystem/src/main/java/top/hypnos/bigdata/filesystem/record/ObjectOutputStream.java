package top.hypnos.bigdata.filesystem.record;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public interface ObjectOutputStream {

    void writeInt(int data) throws IOException;

    void writeBytes(byte[] data) throws IOException;

    default void writeString(String data) throws IOException {
        if (data == null) {
            writeBytes(null);
            return;
        }
        writeBytes(data.getBytes(StandardCharsets.UTF_8));
    }
}
