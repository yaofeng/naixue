package top.hypnos.bigdata.filesystem;

import org.junit.jupiter.api.*;
import top.hypnos.bigdata.filesystem.command.CommandException;
import top.hypnos.bigdata.filesystem.testutils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutionException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MemDatabaseTest {

    private static final File BASE_PATH = new File("./build/test/MemDatabaseTest");
    private static MemDatabase db;

    @BeforeAll
    static void beforeAll() throws IOException {
        FileUtils.clearDirectory(BASE_PATH);
        db = new MemDatabase(BASE_PATH);
    }

    @Test
    @Order(1)
    void createNodeAndGetNode() throws ExecutionException, InterruptedException {
        db.createNode("/unit-test", "hello");
        db.createNode("/unit-test/sub-01", "hello");
        db.createNode("/unit-test/sub-01/sub-sub-01", "hello");

        Assertions.assertEquals("hello", new String(db.getNode("/unit-test").getData(), StandardCharsets.UTF_8));
        Assertions.assertEquals("hello", new String(db.getNode("/unit-test/sub-01").getData(), StandardCharsets.UTF_8));
        Assertions.assertEquals("hello", new String(db.getNode("/unit-test/sub-01/sub-sub-01").getData(), StandardCharsets.UTF_8));

    }

    @Test
    @Order(2)
    void deleteNode() throws ExecutionException, InterruptedException {
        db.deleteNode("/unit-test/sub-01");

        Assertions.assertNotNull(db.getNode("/unit-test"));
        Assertions.assertNull(db.getNode("/unit-test/sub-01"));
        Assertions.assertNull(db.getNode("/unit-test/sub-01/sub-sub-01"));
    }

    @Test
    @Order(3)
    void recover() throws IOException, CommandException {
        final var dbNew = new MemDatabase(BASE_PATH);
        dbNew.recover();

        Assertions.assertNotNull(dbNew.getNode("/unit-test"));
        Assertions.assertNull(dbNew.getNode("/unit-test/sub-01"));
        Assertions.assertNull(dbNew.getNode("/unit-test/sub-01/sub-sub-01"));
    }

    @Test
    @Order(4)
    void snapshot() throws IOException, CommandException {
        db.snapshot();
        final var dbNew = new MemDatabase(BASE_PATH);
        dbNew.recover();

        Assertions.assertNotNull(dbNew.getNode("/unit-test"));
        Assertions.assertNull(dbNew.getNode("/unit-test/sub-01"));
        Assertions.assertNull(dbNew.getNode("/unit-test/sub-01/sub-sub-01"));
    }
}