package top.hypnos.bigdata.filesystem.command;

import org.junit.jupiter.api.*;
import top.hypnos.bigdata.filesystem.FileBinLog;
import top.hypnos.bigdata.filesystem.MemDataTree;
import top.hypnos.bigdata.filesystem.testutils.FileUtils;

import java.io.File;
import java.io.IOException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DeleteNodeCommandExecutorTest {

    private static final File BASE_PATH = new File("./build/test/CreateNodeCommandExecutorTest");
    private MemDataTree tree;

    @BeforeAll
    static void beforeAll() throws IOException {
        FileUtils.clearDirectory(BASE_PATH);
    }

    @BeforeEach
    void setUp() throws CommandException, IOException {
        final var binLog = new FileBinLog(BASE_PATH);
        tree = new MemDataTree(binLog);
        createNode(1, "/unit-test");
        createNode(2, "/unit-test/sub-01");
    }

    @Test
    @Order(1)
    void preExecute() throws CommandException {
        {
            final var command = new CommandImpl<>(3, "/unit-test/sub-01", DeleteNodeCommandExecutor.class);
            command.getExecutor().preExecute(tree, command);
            final var node = command.getExecutor().getNode();
            Assertions.assertNotNull(node);
            Assertions.assertTrue(node.getParent().getChildren().contains(node));
            command.getExecutor().commit();
        }
        {
            final var command = new CommandImpl<>(4, "/unit-test/sub-01", DeleteNodeCommandExecutor.class);
            Assertions.assertThrows(CommandException.class, () -> {
                command.getExecutor().preExecute(tree, command);
            });
        }
    }

    @Test
    @Order(2)
    void commit() throws CommandException {
        final var command = new CommandImpl<>(3, "/unit-test/sub-01", DeleteNodeCommandExecutor.class);
        command.getExecutor().preExecute(tree, command);
        command.getExecutor().commit();
        final var node = command.getExecutor().getNode();
        Assertions.assertNotNull(node);
        Assertions.assertFalse(node.getParent().getChildren().contains(node));
    }

    private void createNode(int txnId, String path) throws CommandException {
        final var command = new CommandImpl<>(txnId, path, CreateNodeCommandExecutor.class);
        command.preExecute(tree);
        command.commit();
    }
}