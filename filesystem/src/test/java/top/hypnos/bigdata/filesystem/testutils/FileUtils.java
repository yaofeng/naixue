package top.hypnos.bigdata.filesystem.testutils;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class FileUtils {

    public static void clearDirectory(final File directory) throws IOException {
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                throw new IOException("无法创建目录：" + directory.getAbsolutePath());
            }
        }

        final var files = directory.listFiles();
        if (files == null) {
            return;
        }
        for (final var file : files) {
            if (file.isDirectory()) {
                clearDirectory(file);
            }
            file.deleteOnExit();
        }
    }
}
