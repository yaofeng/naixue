package top.hypnos.bigdata.filesystem.command;

import org.junit.jupiter.api.*;
import top.hypnos.bigdata.filesystem.FileBinLog;
import top.hypnos.bigdata.filesystem.MemDataTree;
import top.hypnos.bigdata.filesystem.testutils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CreateNodeCommandExecutorTest {

    private static final File BASE_PATH = new File("./build/test/CreateNodeCommandExecutorTest");
    private MemDataTree tree;

    @BeforeAll
    static void beforeAll() throws IOException {
        FileUtils.clearDirectory(BASE_PATH);
    }

    @BeforeEach
    void setUp() throws IOException {
        final var binLog = new FileBinLog(BASE_PATH);
        tree = new MemDataTree(binLog);
    }

    @Test
    @Order(1)
    void getParent() {
        Assertions.assertEquals("", CreateNodeCommandExecutor.getParent("/unit-test"));
        Assertions.assertEquals("/unit-test", CreateNodeCommandExecutor.getParent("/unit-test/hello"));
    }

    @Test
    @Order(2)
    void getLeafPath() {
        Assertions.assertEquals("unit-test", CreateNodeCommandExecutor.getLeafPath("/unit-test"));
        Assertions.assertEquals("hello", CreateNodeCommandExecutor.getLeafPath("/unit-test/hello"));
    }

    @Test
    @Order(3)
    void preExecute() throws CommandException {
        {
            final var command = createExecutor(1, "/unit-test", "hello");
            command.getExecutor().preExecute(tree, command);
            final var node = command.getExecutor().getNode();
            Assertions.assertNotNull(node);
            Assertions.assertEquals(tree.getRoot(), node.getParent());
            Assertions.assertEquals(1, node.getTxnId());
            Assertions.assertEquals("unit-test", node.getPath());
            Assertions.assertEquals("hello", new String(node.getData(), StandardCharsets.UTF_8));
            Assertions.assertFalse(node.getParent().getChildren().contains(node));
            command.getExecutor().commit();
        }

        {
            final var command = createExecutor(2, "/unit-test/sub-01", "hello");
            command.getExecutor().preExecute(tree, command);
            final var node = command.getExecutor().getNode();
            Assertions.assertNotNull(node);
            Assertions.assertEquals(tree.getNode("/unit-test"), node.getParent());
            Assertions.assertEquals(2, node.getTxnId());
            Assertions.assertEquals("sub-01", node.getPath());
            Assertions.assertEquals("hello", new String(node.getData(), StandardCharsets.UTF_8));
            Assertions.assertFalse(node.getParent().getChildren().contains(node));
            command.getExecutor().commit();
        }
    }

    @Test
    @Order(4)
    void commit() throws CommandException {
        // 测试在根节点创建子节点的提交
        final var command0 = createExecutor(1, "/unit-test", "hello");
        command0.getExecutor().preExecute(tree, command0);
        command0.getExecutor().commit();
        final var node0 = command0.getExecutor().getNode();
        Assertions.assertTrue(node0.getParent().getChildren().contains(node0));

        // 测试在非根节点创建子节点的提交
        final var command = createExecutor(2, "/unit-test/sub-01", "hello");
        command.getExecutor().preExecute(tree, command);
        command.getExecutor().commit();
        final var node = command.getExecutor().getNode();
        Assertions.assertTrue(node.getParent().getChildren().contains(node));
    }

    CommandImpl<CreateNodeCommandExecutor> createExecutor(final int txnId, final String path, final String data) {
        return new CommandImpl<>(txnId, path, data.getBytes(StandardCharsets.UTF_8), CreateNodeCommandExecutor.class);
    }
}