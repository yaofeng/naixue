package top.hypnos.bigdata.filesystem;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import top.hypnos.bigdata.filesystem.command.CommandException;
import top.hypnos.bigdata.filesystem.testutils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.ExecutionException;

class FileSnapshotTest {

    private static final File BASE_PATH_SNAPSHOT = new File("./build/test/FileSnapshotTest/snapshot");
    private static final File BASE_PATH_BINLOG = new File("./build/test/FileSnapshotTest/binlog");

    @BeforeAll
    static void beforeAll() throws IOException {
        FileUtils.clearDirectory(BASE_PATH_SNAPSHOT);
        FileUtils.clearDirectory(BASE_PATH_BINLOG);
    }

    @Test
    void saveAndRecover() throws IOException, ExecutionException, InterruptedException, CommandException {
        final var binlog = new FileBinLog(BASE_PATH_BINLOG);
        final var snapshot = new FileSnapshot(BASE_PATH_SNAPSHOT, binlog);

        final var tree = new MemDataTree(binlog);
        tree.createNode("/unit-test", "unit-test");
        tree.createNode("/unit-test/sub-01", "sub-01");
        tree.createNode("/unit-test/sub-02", "sub-01");
        tree.createNode("/unit-test/sub-01/sub-sub-01", "sub-sub-01");
        tree.createNode("/unit-test/sub-01/sub-sub-02", "sub-sub-02");
        tree.deleteNode("/unit-test/sub-01/sub-sub-02");

        snapshot.save(tree);
        final var treeNew = snapshot.recover();

        Assertions.assertNotNull(treeNew.getNode("/unit-test"));
        Assertions.assertNotNull(treeNew.getNode("/unit-test/sub-01"));
        Assertions.assertNotNull(treeNew.getNode("/unit-test/sub-02"));
        Assertions.assertNotNull(treeNew.getNode("/unit-test/sub-01/sub-sub-01"));
        Assertions.assertNull(treeNew.getNode("/unit-test/sub-01/sub-sub-02"));

        tree.deleteNode("/unit-test/sub-01");
        tree.createNode("/unit-test/sub-02/sub-sub-01", "sub-sub-01");
        tree.createNode("/unit-test/sub-02/sub-sub-02", "sub-sub-01");

        final var treeNew2 = snapshot.recover();

        Assertions.assertNotNull(treeNew2.getNode("/unit-test"));
        Assertions.assertNotNull(treeNew2.getNode("/unit-test/sub-02"));
        Assertions.assertNotNull(treeNew2.getNode("/unit-test/sub-02/sub-sub-01"));
        Assertions.assertNotNull(treeNew2.getNode("/unit-test/sub-02/sub-sub-02"));
        Assertions.assertNull(treeNew2.getNode("/unit-test/sub-01"));
    }
}