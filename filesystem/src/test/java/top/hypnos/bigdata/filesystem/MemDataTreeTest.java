package top.hypnos.bigdata.filesystem;

import org.junit.jupiter.api.*;
import top.hypnos.bigdata.filesystem.testutils.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutionException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MemDataTreeTest {

    private static final File BASE_PATH = new File("./build/test/MemDataTreeTest/binlog");

    private MemDataTree tree;

    @BeforeAll
    static void beforeAll() throws IOException {
        FileUtils.clearDirectory(BASE_PATH);
    }

    @BeforeEach
    void setUp() throws IOException {
        final var binlog = new FileBinLog(BASE_PATH);
        tree = new MemDataTree(binlog);
    }

    @Test
    @Order(1)
    void createNodeAndGetNode() throws ExecutionException, InterruptedException {
        tree.createNode("/unit-test", "hello");
        tree.createNode("/unit-test/sub-01", "hello");
        tree.createNode("/unit-test/sub-01/sub-sub-01", "hello");

        Assertions.assertEquals("hello", new String(tree.getNode("/unit-test").getData(), StandardCharsets.UTF_8));
        Assertions.assertEquals("hello", new String(tree.getNode("/unit-test/sub-01").getData(), StandardCharsets.UTF_8));
        Assertions.assertEquals("hello", new String(tree.getNode("/unit-test/sub-01/sub-sub-01").getData(), StandardCharsets.UTF_8));

    }

    @Test
    @Order(2)
    void deleteNode() throws ExecutionException, InterruptedException {
        tree.createNode("/unit-test", "hello");
        tree.createNode("/unit-test/sub-01", "hello");
        tree.createNode("/unit-test/sub-01/sub-sub-01", "hello");
        tree.deleteNode("/unit-test/sub-01");

        Assertions.assertNotNull(tree.getNode("/unit-test"));
        Assertions.assertNull(tree.getNode("/unit-test/sub-01"));
        Assertions.assertNull(tree.getNode("/unit-test/sub-01/sub-sub-01"));
    }
}