package top.hypnos.bigdata.filesystem;

import org.junit.jupiter.api.*;
import top.hypnos.bigdata.filesystem.command.CommandImpl;
import top.hypnos.bigdata.filesystem.command.CreateNodeCommandExecutor;
import top.hypnos.bigdata.filesystem.command.DeleteNodeCommandExecutor;
import top.hypnos.bigdata.filesystem.testutils.FileUtils;

import java.io.File;
import java.io.IOException;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class FileBinLogTest {

    private static final File BASE_PATH = new File("./build/test/FileBinLogTest/binlog");

    @BeforeAll
    static void beforeAll() throws IOException {
        FileUtils.clearDirectory(BASE_PATH);
    }

    @Test
    @Order(1)
    void append() throws IOException {
        final var binlog = new FileBinLog(BASE_PATH);
        for (int i = 1; i <= 1000; i++) {
            final var command = new CommandImpl<>(i, "/aaa/" + i, CreateNodeCommandExecutor.class);
            binlog.append(command);
        }
        for (int i = 1001; i <= 2000; i++) {
            final var command = new CommandImpl<>(i, "/aaa/" + i, DeleteNodeCommandExecutor.class);
            binlog.append(command);
        }

        Assertions.assertTrue(new File(BASE_PATH, "binlog-001.bin").exists());
        Assertions.assertTrue(new File(BASE_PATH, "binlog-002.bin").exists());
        Assertions.assertFalse(new File(BASE_PATH, "binlog-003.bin").exists());

    }

    @Test
    @Order(2)
    void loadAfter() throws IOException {
        final var binlog = new FileBinLog(BASE_PATH);
        final var indexFrom = 10;
        final var commands = binlog.loadAfter(indexFrom);
        Assertions.assertEquals(2000 - indexFrom, commands.size());

    }
}