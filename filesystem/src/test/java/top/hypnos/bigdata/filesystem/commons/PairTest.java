package top.hypnos.bigdata.filesystem.commons;

import org.junit.jupiter.api.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class PairTest {

    private Pair<Integer, String> instance;

    @BeforeEach
    void setup() {
        final var a = 1;
        final var b = "hello";
        instance = Pair.of(a, b);
    }

    @Test
    @Order(1)
    void getA() {
        Assertions.assertEquals(1, instance.getA());
    }

    @Test
    @Order(2)
    void getB() {
        Assertions.assertEquals("hello", instance.getB());
    }
}