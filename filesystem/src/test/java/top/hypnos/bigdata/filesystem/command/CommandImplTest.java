package top.hypnos.bigdata.filesystem.command;

import org.junit.jupiter.api.*;
import top.hypnos.bigdata.filesystem.record.ObjectInputStreamImpl;
import top.hypnos.bigdata.filesystem.record.ObjectOutputStreamImpl;
import top.hypnos.bigdata.filesystem.testutils.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class CommandImplTest {

    private static final File BASE_PATH = new File("./build/test/CommandImplTest");
    private static final File SERIALIZE = new File(BASE_PATH, "serialize.bin");

    @BeforeAll
    static void beforeAll() throws IOException {
        FileUtils.clearDirectory(BASE_PATH);
    }

    @Test
    @Order(1)
    void serialize() throws IOException {
        final var command1 = new CommandImpl<>(1, "/aaa", "aaa".getBytes(StandardCharsets.UTF_8), CreateNodeCommandExecutor.class);
        final var command2 = new CommandImpl<>(2, "/bbb", "bbb".getBytes(StandardCharsets.UTF_8), DeleteNodeCommandExecutor.class);
        final var command3 = new CommandImpl<>(3, "/ccc", "ccc".getBytes(StandardCharsets.UTF_8), CreateNodeCommandExecutor.class);
        try (final var output = new FileOutputStream(SERIALIZE)) {
            command1.serialize(new ObjectOutputStreamImpl(output));
            command2.serialize(new ObjectOutputStreamImpl(output));
            command3.serialize(new ObjectOutputStreamImpl(output));
        }
    }

    @Test
    @Order(2)
    void deserialize() throws IOException {
        final var command1 = new CommandImpl<>();
        final var command2 = new CommandImpl<>();
        final var command3 = new CommandImpl<>();
        try (final var input = new FileInputStream(SERIALIZE)) {
            command1.deserialize(new ObjectInputStreamImpl(input));
            command2.deserialize(new ObjectInputStreamImpl(input));
            command3.deserialize(new ObjectInputStreamImpl(input));
        }

        Assertions.assertEquals(1, command1.getTxnId());
        Assertions.assertEquals("/aaa", command1.getPath());
        Assertions.assertArrayEquals("aaa".getBytes(StandardCharsets.UTF_8), command1.getData());
        Assertions.assertEquals(CreateNodeCommandExecutor.class, command1.getExecutor().getClass());

        Assertions.assertEquals(2, command2.getTxnId());
        Assertions.assertEquals("/bbb", command2.getPath());
        Assertions.assertArrayEquals("bbb".getBytes(StandardCharsets.UTF_8), command2.getData());
        Assertions.assertEquals(DeleteNodeCommandExecutor.class, command2.getExecutor().getClass());

        Assertions.assertEquals(3, command3.getTxnId());
        Assertions.assertEquals("/ccc", command3.getPath());
        Assertions.assertArrayEquals("ccc".getBytes(StandardCharsets.UTF_8), command3.getData());
        Assertions.assertEquals(CreateNodeCommandExecutor.class, command3.getExecutor().getClass());
    }
}